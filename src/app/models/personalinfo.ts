export class Personalinfo {
    first_name: string = '';
    second_name: string= '';
    third_name: string= '';
    family_name: string= '';
    gender: string= '';
    national_id: string= '';
    place_national: string= '';
    place_birth: string= '';
    date_birth: string= '';
    job: string='';
}

