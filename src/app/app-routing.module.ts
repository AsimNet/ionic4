import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./landing/landing.module').then( m => m.LandingPageModule)
  },
  {
    path: 'landing',
    loadChildren: () => import('./landing/landing.module').then( m => m.LandingPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'login-commit',
    loadChildren: () => import('./pages/auth/login-commit/login-commit.module').then( m => m.LoginCommitPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'personalinfo',
    loadChildren: () => import('./pages/form/personalinfo/personalinfo.module').then( m => m.PersonalinfoPageModule)
  },
  {
    path: 'personal-info',
    loadChildren: () => import('./pages/form/personal-info/personal-info.module').then( m => m.PersonalInfoPageModule)
  },
  {
    path: 'job',
    loadChildren: () => import('./pages/form/job/job.module').then( m => m.JobPageModule)
  },
  {
    path: 'social-info',
    loadChildren: () => import('./pages/form/social-info/social-info.module').then( m => m.SocialInfoPageModule)
  },
  {
    path: 'contact-info',
    loadChildren: () => import('./pages/form/contact-info/contact-info.module').then( m => m.ContactInfoPageModule)
  },
  {
    path: 'home-info',
    loadChildren: () => import('./pages/form/home-info/home-info.module').then( m => m.HomeInfoPageModule)
  },
  {
    path: 'amina',
    loadChildren: () => import('./pages/form/amina/amina.module').then( m => m.AminaPageModule)
  },
  {
    path: 'commitment',
    loadChildren: () => import('./pages/form/commitment/commitment.module').then( m => m.CommitmentPageModule)
  },
  {
    path: 'attachment',
    loadChildren: () => import('./pages/form/attachment/attachment.module').then( m => m.AttachmentPageModule)
  },
  {
    path: 'camera',
    loadChildren: () => import('./pages/form/camera/camera.module').then( m => m.CameraPageModule)
  },

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
