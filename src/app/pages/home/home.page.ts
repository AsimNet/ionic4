import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  user: User;

  constructor(
    private modalController: ModalController,
    private navCtrl: NavController,
    private authService: AuthService

  ) { }

  ngOnInit() {
  }
    // انخلطت مع ضفحة البيرسنال

  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        this.user = user;
      }
    );
  }
  dismissPage() {
    this.modalController.dismiss();
  }
  // pushPage(): void{
  //   // this.dismissPage();
  //   this.modalController.dismiss();
  //   this.navCtrl.navigateForward(PersonalinfoPage);
  // }


  // async personalModal() {
  //   this.dismissPage();
  //   const personalModal = await this.modalController.create({
  //     component: PersonalinfoPage
  //   });
  //   return await personalModal.present();
  // }

}
