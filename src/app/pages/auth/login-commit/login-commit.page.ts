import { Component, OnInit } from '@angular/core';

import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-login-commit',
  templateUrl: './login-commit.page.html',
  styleUrls: ['./login-commit.page.scss'],
})
export class LoginCommitPage implements OnInit {

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() {
  }
  dismissLogin() {
    this.modalController.dismiss();
  }

}
