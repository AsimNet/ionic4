import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginCommitPage } from './login-commit.page';

const routes: Routes = [
  {
    path: '',
    component: LoginCommitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginCommitPageRoutingModule {}
