import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AminaPageRoutingModule } from './amina-routing.module';

import { AminaPage } from './amina.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AminaPageRoutingModule
  ],
  declarations: [AminaPage]
})
export class AminaPageModule {}
