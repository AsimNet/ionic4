import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AminaPage } from './amina.page';

describe('AminaPage', () => {
  let component: AminaPage;
  let fixture: ComponentFixture<AminaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AminaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AminaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
