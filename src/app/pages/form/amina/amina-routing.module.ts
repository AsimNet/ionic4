import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AminaPage } from './amina.page';

const routes: Routes = [
  {
    path: '',
    component: AminaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AminaPageRoutingModule {}
