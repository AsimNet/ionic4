import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SocialInfoPage } from './social-info.page';

const routes: Routes = [
  {
    path: '',
    component: SocialInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SocialInfoPageRoutingModule {}
