import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SocialInfoPage } from './social-info.page';

describe('SocialInfoPage', () => {
  let component: SocialInfoPage;
  let fixture: ComponentFixture<SocialInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SocialInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
