import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from "src/app/services/http-service.service";
import { map } from "rxjs/operators";

@Component({
  selector: 'app-attachment',
  templateUrl: './attachment.page.html',
  styleUrls: ['./attachment.page.scss'],
})
export class AttachmentPage implements OnInit {

  private file: File

  constructor(
    private httpService : HttpServiceService,
    ) { }

  ngOnInit() {
    
  }
  onFileChange(fileChangeEvent){
    this.file=fileChangeEvent.target.files[0];

  }
  

  async submitForm( ) {
    let formData =new FormData();
    formData.append('file', this.file,this.file.name);
    return this.httpService.makePost('auth/form', formData, true).pipe(map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res;
      },
      error => {
        console.log("error stornig")
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        return error.json();
      }
    ));
  }



}
