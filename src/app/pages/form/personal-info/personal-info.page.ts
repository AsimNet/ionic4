import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.page.html',
  styleUrls: ['./personal-info.page.scss'],
})
export class PersonalInfoPage implements OnInit {
  public personal_info : {};

  constructor(
    private navCtrl: NavController,
  ) { }

  ngOnInit() {
  }
  addInfo() {
    console.log(this.personal_info)
  }
}
