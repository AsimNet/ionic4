import { Component, OnInit } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { Personalinfo } from '../../../models/personalinfo';


@Component({
  selector: 'app-personalinfo',
  templateUrl: './personalinfo.page.html',
  styleUrls: ['./personalinfo.page.scss'],
})
export class PersonalinfoPage implements OnInit {
  public personal_info : any;

  constructor(
    public navParams: NavParams,
  ) {
    this.personal_info = this.navParams.get('personal_info') || new Personalinfo();
   }

  ngOnInit() {
  }

  addInfo() {
    console.log(this.personal_info)
  }

}
