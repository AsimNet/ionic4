import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {

  constructor(
    private http : Http
    ) { }
  private url = 'http://localhost:8000/api/';

  public makePost( endpoint : string, data : any, authenticated : boolean = false ) {
    return this.http.post( `${this.url}${endpoint}`, data, { headers : this.getHeaders(authenticated) } );
  }
  
  public makePut( endpoint : string, data : any, authenticated : boolean = false ) {
    return this.http.put( `${this.url}${endpoint}`, data, { headers : this.getHeaders(authenticated) } );
  }

  public makeGet( endpoint : string, authenticated : boolean = false ) {
    return this.http.get( `${this.url}${endpoint}`, { headers : this.getHeaders(authenticated) } );
  }

  private getHeaders(authenticated : boolean){
    let headers = new Headers();
    let token = localStorage.getItem('token');
    headers.append( 'Content-Type', 'application/json' );
    if ( authenticated ) {
      headers.append( 'Authorization', token);
    }
    return headers;
  }
}
